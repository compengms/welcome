# Computer Engineering MS

Collection of useful material, notes, exercises, exams, homework and much more.

## How to contribute

### Add material

You can open a PR to the corresponding project, using the proper label: e.g. `notes` or `exams`.

### Report missing our outdated material

Open a new issue using the proper label: e.g. `outadated` for deprecated resources.

## List of courses

Note: If some course is missing [click here](https://gitlab.com/compengms/welcome/-/issues/new) to report.

- [Advanced Operating Systems and Virtualization](https://gitlab.com/compengms/advanced-operating-systems-virtualization)
- [Algorithm Design](https://gitlab.com/compengms/algorithm-design)
- [Computer and Network Security](https://gitlab.com/compengms/computer-network-security)
- [Data Management](https://gitlab.com/compengms/data-management)
- [Distributed Systems](https://gitlab.com/compengms/distributed-systems)
- [Capacity Planning](https://gitlab.com/compengms/capacity-planning)
- [Cryptography](https://gitlab.com/compengms/cryptography)
- [Formal Methods](https://gitlab.com/compengms/formal-methods)
- [Human Computer Interaction](https://gitlab.com/compengms/human-computer-interaction)
- [Interactive Graphics](https://gitlab.com/compengms/interactive-graphics)
- [Machine Learning](https://gitlab.com/compengms/machine-learning)
- [Mobile Applications and Cloud Computing](https://gitlab.com/compengms/mobile-applications-cloud-computing)
- [Network Infrastructures](https://gitlab.com/compengms/network-infrastructures)
- [Practical Network Defense](https://gitlab.com/compengms/practical-network-defense)
- [Secure Computation](https://gitlab.com/compengms/secure-computation)
- [Security Governance](https://gitlab.com/compengms/security-governance)
- [Software Engineering](https://gitlab.com/compengms/software-engineering)
- [Visual Analytics](https://gitlab.com/compengms/visual-analytics)
- [Web Information Retrieval](https://gitlab.com/compengms/web-information-retrieval)
- [Web Security and Privacy](https://gitlab.com/compengms/web-security-privacy)

